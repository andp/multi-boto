const Discord = require('discord.js');
const bot = new Discord.Client();
const needle= require('needle');
const cfg= require('./'+process.argv[2]);

const channels= cfg.listenserv.map(val => val.listen); //shove all listen keys into an arr
const listenserv= cfg.listenserv; //pull from cfg and store

bot.on('ready', function() {
  console.log(`${cfg.name} logged in`);
  post(`${cfg.name} listener running`);
});

bot.on('disconnect', () => {
  console.log(`bot DCed`);
  post(`${cfg.name} bot has DCed`);
});

bot.on('message', message => {
  if (!message)
    return;
  console.log('got message');
  if(message.channel) {
    channels.map(i => {
      console.log(message.channel);
      if(i== message.channel.id) { //only run if matches a channel
        console.log(message.content);
        let user= message.member ? message.member.user.username : '';
        if(message.member.user.nickname !== null && message.member.user.nickname !== void 0) {
          user= message.member.user.nickname;
        }
        if(message.attachments.array().length)
          message.attachments.array().map(attachment => post(`${attachment.url}\nSent by ${user}`, i))
        return post(`${message.content}\nSent by ${user}`, i)
      }
    });
  }
});

/*
 * Send the data to the listener bot that then relays it
 * into discord
 *
 * TEXT - data from message
 * CHANNEL - channel ID
 */
//post pings
let post = (text, channel) => {
  let data = {
    text: text,
    name: cfg.name,
    colour: cfg.colour,
    channels: channel ? listenserv.filter(val => channel== val.listen)[0].serve : cfg.servers[0] //send to specific channel or default sending to general
  };
  needle.request('get', `http://${cfg.ip}:3000/relay/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(`${resp.body} - ping posted`); // here you go, mister.
  });
}

console.log(process.argv[2]);

bot.login(cfg.token);
