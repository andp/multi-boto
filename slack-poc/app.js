const slackAPI = require('slackbotapi');
const unescapeJs = require('unescape-js');  //dumb to have both but it works in making it
const unescape = require('lodash.unescape');//look pretty
const needle= require('needle');
const cfg= require('./'+process.argv[2]);

const channels= cfg.listenserv.map(val => val.listen); //shove all listen keys into an arr
const listenserv= cfg.listenserv; //pull from cfg and store
const filter= cfg.filter;

var users;//hold user data for server

let api= new slackAPI({
  'token': cfg.token,
  'logging': false,
  'autoReconnect': true
});

//listen for message
api.on('message', data => {
  if (typeof data.text === 'undefined') return; //return out if no data
  if(!data.subtype|| (data.bot_id&& data.text)|| data.subtype== "slackbot_response") {
    channels.map(i => {
      if(i== data.channel) { //only run if matches a channel
        console.log(data)
        var name;
        let text= data.text;
        try { //try to assign real_name, throw it as null if it doesn't work
          name= findUser(data.user)[0].real_name;
        } catch (ex) {name= null}
        if(text.match(/<@U\S*>/))
          text= userFill(text)
        return post(`${unescapeJs(unescape(text))}\nSent by ${name}`, i)
      }
    });
  }
});

//Send message on connect
api.on('hello', data => {
  console.log(`${cfg.name} logged in`);
  users= api.slackData.users; //grab all the users data and throw it into a variable
  post(`${cfg.name} listener running`);
});

//Send message on DC
api.on('close', data => {
  console.log(`bot DCed`);
  post(`${cfg.name} bot has DCed`);
});

//translate <@UID> to <@username>
let userFill = otext => {
  let text= otext;
  try {
    let name= text.match(/<@U\S*>/g);
    name.map(i => {
      let stripname= i.replace(/<|@|>/g, '');
    	text= text.replace(stripname, findUser(stripname)[0].real_name)
    })
    return text;
  } catch (ex) {return text;}
}

//find user based on passed in ID
let findUser = id => users.filter(user => user.id === id);

//post pings
let post = (text, channel) => {
  let data = {
    text: text,
    name: cfg.name,
    colour: cfg.colour,
    channels: channel ? listenserv.filter(val => channel== val.listen)[0].serve : cfg.servers[0] //send to specific channel or default sending to general
  };
  needle.request('get', `http://${cfg.ip}:3000/relay/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(`${resp.body} - ping posted`); // here you go, mister.
  });
}
