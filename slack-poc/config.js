module.exports = {
  //Name of bot, should be set to what server/group it's lisening to
  name: 'NAME',

  //IP URL it's going to
  ip: '127.0.0.1',

  listenserv: [
    {
      'listen': 'LISTENCHAN',
      'serve': 'SERVCHAN'
    }
  ],

  //ID of the Discord channel(s) you want to relay the message to
  //this is a string
  servers: ['SERVSERVER'],

  //Colour you want to embed to be
  colour: '22807',

  //Set to true when there isn't a dedicated ping channel
  filter: false,

  // The slack token
  token: 'TOKEN'
};
