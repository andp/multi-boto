const slackAPI = require('slackbotapi');
const unescapeJs = require('unescape-js');  //dumb to have both but it works in making it
const unescape = require('lodash.unescape');//look pretty
const needle= require('needle');
const cfg= require('./'+process.argv[2]);

const channels= cfg.channels;
const filter= cfg.filter;

var api= new slackAPI({
  'token': cfg.token,
  'logging': true,
  'autoReconnect': true
});

api.on('message', (data) => {
  console.log(data);
  if (typeof data.text === 'undefined') return; //return out if no data
  if(!data.subtype|| (data.bot_id&& data.text)|| data.subtype== "slackbot_response") {
    console.log(data.user);
    //get name of sender
    //let name= api.getUser(data.user) ? api.getUser(data.user).name : undefined;
    //brave lobby relay
    if(cfg.name.toLowerCase()== 'brave'&& data.channel== 'C0CPDJ9L4') {
      brave(`${api.getUser(data.user) ? api.getUser(data.user).name : undefined}: ${unescapeJs(unescape(data.text))}`)
    }
    if(filter&& (data.text.match(`!channel`)|| data.text.match(`!group`)|| data.text.match(`!everyone`)|| data.text.match(`!here`)) )
      return channels.map(i => i== data.channel ? post(`${unescapeJs(unescape(data.text))}\nSent by ${api.getUser(data.user) ? api.getUser(data.user).name : undefined}`) : null)
    else if(!filter)
      return channels.map(i => i== data.channel ? post(`${unescapeJs(unescape(data.text))}\nSent by ${api.getUser(data.user) ? api.getUser(data.user).name : undefined}`) : null)
  }
});

//Send message on connect
api.on('hello', (data) => {
  console.log(`${cfg.name} logged in`);
  post(`${cfg.name} listener running`);
  serviceUpdate(true);
});

//Send message on DC
api.on('close', (data) => {
  console.log(`bot DCed`);
  post(`${cfg.name} bot has DCed`);
  serviceUpdate(false);
});

//post pings
let post= function(text) {
  let data = {
    text: text,
    name: cfg.name,
    colour: cfg.colour,
    channels: cfg.servers
  };
  needle.request('get', `http://${cfg.ip}:3000/relay/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(resp.body); // here you go, mister.
  });
}

//function to relay out lobby
let brave= function(text) {
  let data = {
    text: text,
    name: cfg.name,
    colour: cfg.colour,
    channels: cfg.servers
  };
  needle.request('get', `http://${cfg.ip}:3000/brave/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(resp.body); // here you go, mister.
  });
}

//update bot monitor
let serviceUpdate= function(status) {
  let data= {
    name: cfg.name,
    service: 'Slack',
    status: status ? 'online' : 'offline'
  }
  needle.request('post', `${cfg.monitor}/status/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(resp.body);
  });
}
