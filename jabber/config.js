module.exports = {
  name: 'NAME',
  ip: 'IP',
  servers: ['DISCORDCHANNELIDS'],
  colour: '15382534',
  //Do pings get PMed to you or are they in a channel?
  pmpings: true,
  jname: 'name',
  jid: 'name@server/name',
  password: 'pass',
  //name of room to listen to, doesn't matter if they
  //pm you pings
  room_jid: null,
  host: 'server'
};