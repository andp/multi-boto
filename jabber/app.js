var XMPP = require('node-xmpp-client');
const http = require('http');
const needle= require('needle');
const cfg= require('./'+process.argv[2]);

let post= function(text) {
  let data = {
    text: text,
    name: cfg.name,
    colour: cfg.colour,
    channels: cfg.servers
  };
  needle.request('get', `http://${cfg.ip}:3000/relay/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(resp.body); // here you go, mister.
  });
}

let serviceUpdate= function(status) {
  let data= {
    name: cfg.name,
    service: 'Jabber',
    status: status ? 'online' : 'offline'
  }
  needle.request('post', `http://${cfg.monitor}/status/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(resp.body);
  });
}

const JID = cfg.jid;
const PASSWORD = cfg.password;
const ROOM_JID = cfg.room_jid;
const HOST = cfg.host;
const PORT = 5222;

//Random XMPP ID string ; some setups need this
const RANDOM_ID = Math.random().toString(36).substring(2,10);

//The name this bot will use in your XMPP Chatroom.
//You can change this to whatever you want
const IN_CHANNEL_BOT_NAME = cfg.jname;

//Debugging options
const DEBUG = false; //turn on the debug log. Useful to troubleshoot issues.
const LOG_FILE = './debug.log'; //where the log file will be saved to. Same folder this file lives in by default
var xmppKeepAlivesSent = 0; //Keep track of XMPP keepAlives sent since the last message
var fs = null;

//initialize node-xmpp-client
var xmpp = new XMPP({
	jid: JID,
	password: PASSWORD,
	preferredSaslMechanism: 'PLAIN',
	preferred: 'PLAIN',
	reconnect: true,
	host: HOST,
	port: PORT
});

//Login to XMPP server, and join the chatroom
xmpp.on('online', function() {
	//console.log("Successfully connected to XMPP chatroom '" + ROOM_JID + '\'.');
	//if (DEBUG) addLog("[XMPP] Successfully connected to XMPP chatroom '" + ROOM_JID);

	//let the world know we're online, and update our status accordingly
	xmpp.send(new XMPP.Stanza('presence', {type: 'available'}));

	//join the Multi-User Chatroom
	if(!cfg.pmpings)
    xmpp.send(new XMPP.Element('presence', {from: JID, to: ROOM_JID + '/' + IN_CHANNEL_BOT_NAME}));
  post(`${cfg.name} listener running`);
  serviceUpdate(true);
});

xmpp.on('auth', function(){
	console.log('Successfully authenticated with ' + JID);
	if (DEBUG) addLog('[XMPP] Successfully authenticated with ' + JID);
});

xmpp.on('stanza', function(stanza) {
	//@TODO ignore all history -- kinda dirty, but XMPP keeps throwing chat history stanzas despite telling it otherwise
	//or I'm a n00b, likely the latter
	var isHistory = false;
	stanza.children.forEach(function(element){
		if (element.name === 'delay')//if the name of any child element is 'delay', the stanza is chat history
			isHistory = true;
	});
	if (isHistory) return;

	// ignore everything that isn't from the chatroom, or came from the bot itself
	if (stanza.is('message')) {
		var body = stanza.getChild('body');
		if(stanza.attrs.type === 'groupchat')
			console.log(`from group chat`);
		//stanzas with empty bodys are topic changes, and are ignored
		if(cfg.pmpings&& stanza.attrs.type === 'groupchat') {
			return;
		}
		if (!body) return;

		var bodyText = body.getText();
		console.log(bodyText);
		post(bodyText);
	}

});

xmpp.on('error', function(e) {
	console.log("\nSomething went wrong with XMPP:\n");
	post(`Something broke with ${cfg.name} jabber`);
  serviceUpdate(false);
	console.log(e);
});

//XMPP Server keep-alive
setInterval(function(){
	xmpp.send(' ');
	if (DEBUG) xmppKeepAlivesSent++;
}, 60000);

//XMPP Server keep-alive
setInterval(function(){
	xmpp.send(' ');
	//if (DEBUG) addLog('[XMPP] ' + xmppKeepAlivesSent + ' keepAlives sent since the last message sent/received by XMPP');
	//if (DEBUG) addLog('[XMPP] ' + ((xmpp.state == 5) ? 'Connection OK' : 'Problem with connection') + ' (State: ' + xmpp.state + ')');
}, 1800000); //every half hour

//on close, set XMPP status to offline and logout of discord
process.on('SIGINT', function(code) {
	console.log('\nShutting down...');
	setTimeout(function() {
		process.exit()
	}, 1000);
});


//Debug helpers
function formatLogEntry(inputString){
	if (inputString.length) return '\n' + Date() + ': ' + inputString;
}

function addLog(inputString){
	if (fs){
		fs.appendFile(LOG_FILE, formatLogEntry(inputString), function(err){
			if (err) console.log(formatLogEntry('\nCouldn\'t write to the log file for some reason:\n' + err));
		});
	}
}
