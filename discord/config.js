module.exports = {
  //Name of bot, should be set to what server/group it's lisening to
  name: 'NAME',

  //IP/URL it's going to
  ip: '127.0.0.1',

  //IP/URL of monitor server
  monitor: '127.0.0.1',

  //Array of discord channel IDs to listen to
  channels: ['CHANNELID'],

  //ID of the Discord channel(s) you want to relay the message to
  //this is a string
  servers: ['DESTINATIONCHANNELID'],
  //this isn't the greatest naming convention tbh

  //Colour you want to embed to be
  colour: '22807',

  // The discord token
  token: 'TOKEN'
};
