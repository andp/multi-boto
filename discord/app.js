const Discord = require('discord.js');
const http = require('http');
const bot = new Discord.Client();
const needle= require('needle');
const cfg= require('./'+process.argv[2]);

const channels= cfg.channels;

bot.on('ready', function() {
  console.log(`${cfg.name} logged in`);
  post(`${cfg.name} listener running`);
  serviceUpdate(true);
});

bot.on('disconnect', () => {
  console.log(`bot DCed`);
  post(`${cfg.name} bot has DCed`);
  serviceUpdate(false);
});

bot.on('message', message => {
  if (!message)
    return;
  if(message.channel)
    return channels.map(i => i== message.channel.id ? post(`${message.content}\nSent by ${message.member ? message.member.user.username : null}`) : null)
});

/*
 * Update the monitor with the status of the bot
 *
 * STATUS - true for online, false for offline
 */
let serviceUpdate= function(status) {
  let data= {
    name: cfg.name,
    service: 'Discord',
    status: status ? 'online' : 'offline'
  }
  needle.request('post', `http://${cfg.monitor}/status/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(resp.body);
  });
}

/*
 * Send the data to the listener bot that then relays it
 * into discord
 *
 * TEXT - data from message
 */
let post= function(text) {
  let data = {
    text: text,
    name: cfg.name,
    colour: cfg.colour,
    channels: cfg.servers
  };
  // can change port + path to whatever you want,
  // not a config option though it could be one
  needle.request('get', `http://${cfg.ip}:3000/relay/`, data, function(err, resp) {
    if (!err && resp.statusCode == 200)
      console.log(resp.body); // here you go, mister.
  });
}

console.log(process.argv[2]);

bot.login(cfg.token);
