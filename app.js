const Discord = require("discord.js");
const bot = new Discord.Client();

/* Express stuff */
/*
 * Probably a much lighter weight routing/HTTP library than
 * express, but I haven't bothered looking much as I've used
 * express a lot
 */
const express = require("express");
const app = express();

const splitAt = index => x => [x.slice(0, index), x.slice(index)];

const hasLink = text => {
  const links = text.match(/(https?:\/\/[^\s]+)/g);
  return links && links;
};

//can probably change to POST but >semantics
app.get("/relay", function(req, res) {
  const text = req.query.text.replace(/`/g, "").replace(/~/g, "");
  const channels = req.query.channels;

  const embed = new Discord.RichEmbed({
    color: req.query.colour,
    title: req.query.name.toUpperCase(),
    description: text
  });

  //send to all channels that were sent in get
  console.log(req.query.channels);
  if (text.length > 1900) {
    return splitAt(1900)(text).map(i => {
      embed.setDescription(i);
      bot.channels.get(channels).send({ embed });
    });
  }

  try {
    bot.channels.get(channels).send({ embed });
  } catch (ex) {
    console.log(ex);
    bot = new Discord.Client();
  }
  try {
    const links = hasLink(text);
    if (links) {
      bot.channels.get(channels).send(links.join(" "));
    }
  } catch (ex) {
    console.log("something wrong with hasLink");
    console.log(ex);
  }
  //Send to confirm that it was received
  res.send("GET");
});

app.listen(3000, () => console.log(`Listening on port 3000!`));
bot.on("ready", () => console.log(`I am ready!`));
bot.on("disconnect", () => console.log(`bot DCed`));
bot.login(process.env.MAINTOKEN);
